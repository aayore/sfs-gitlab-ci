## Software Freedom School

<div align="right"><a href="https://gitlab.com/aayore/sfs-gitlab-ci/commits/master"><img alt="pipeline status" src="https://gitlab.com/aayore/sfs-gitlab-ci/badges/master/pipeline.svg" /></a></div>

# Continuous Integration with GitLab CI  

The easiest way to use these class materials is to follow this link:

### [Slides](https://aayore.gitlab.io/sfs-gitlab-ci)

It will take you through the class with links out to the labs.

---

### Using locally:

Check out [.gitlab-ci.yml](.gitlab-ci.yml) to see how it's built.

Basically, it uses the [Darkslide](https://pypi.python.org/pypi/darkslide) Python module to transpile markdown to a nice slideshow.

Links to labs are hard-coded, so you'll have to update those manually.

```
pip install darkslide
mkdir public
darkslide slides --destination=public/index.html --embed
open public/index.html
```
