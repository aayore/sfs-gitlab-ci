# Beginning Continuous Integration

> Good Morning!
>
> Since it's your first day on the new team, I'd like to suggest that you guys take a look at this "continuous integration" thing.  It seems like we could benefit from it.  Take a copy of my sample app to start with.  It's pretty basic, but it should give you what you need to get started.
>
> I'd like to see you create some unit tests and integration tests that will verify basic functionality.
>
> Thanks!

---

### Stuff to do

1. Create a GitLab project called `my-sfs-gitlab-ci-demo` (see image below)
3. Clone `my-sfs-gitlab-ci-demo` locally
52. For each of these files, you'll need to make them executable: `chmod +x say.sh`, for example
49. To run these files in a terminal:

        ./say.sh
        ./say.sh something
        ./myapp.sh
        ./myapp.sh something

7. Save this as `say.sh` in your repo:

        #!/bin/sh

        echo $@

276. Save this as `myapp.sh` in your repo:

          #!/bin/sh

          case $1 in
            "")
              echo "Hello World"
              ;;
            *)
              ./say.sh $@
              ;;
          esac

2. Test them manually!
5. Save this as `unit_test.sh` in your repo:

        #!/bin/sh

        if [ "$(./myapp.sh)" == "Hello World" ]
          then
            echo "PASS"
          else
            echo "FAIL"
            exit 1
        fi


1. Save this as `integration_test.sh` in your repo:

        #!/bin/sh

        function test_the_app {
          echo "Testing with: $@"
          if [ "$(./myapp.sh $@)" == "$@" ]
            then
              echo "PASS"
            else
              echo "FAIL"
              exit 1
          fi
        }

        test_the_app "Hello"
        test_the_app "nothing"


---

### With a Partner

- Review each others' test scripts
- Can you come up with a scenario that breaks the tests?  (Yields false results...)

---

![lab02_new_project_menu.png](/images/lab02_new_project_menu.png)

---

|Previous: [Intro & Setup](/labs/01_intro_and_prereq_setup.md)|Next: [Tests at Gitlab](/labs/03_tests_at_gitlab.md)|
|---:|:---|
