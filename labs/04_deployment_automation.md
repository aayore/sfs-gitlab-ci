# Deployment Automation

> I know deployments can take a variety of forms.  We don't have to solve for all potential problems right now.  We just need a placeholder we can build on later.  For a basic website, I think you can use GitLab Pages.

---

### Stuff to do

1. Create a new project on GitLab.
5. Make a `public` directory in your repo.
5. Put some text in an `index.html` file in that `public` directory.
5. Add a `.gitlab-ci.yml` file to the root of your repository.
    - Almost any image will work (e.g. `alpine:3.7` or `python:3`).  Smaller is faster.
    - You must have a `pages` job with `artifacts.paths = [ public ]`

---

### Bare minimum example

Omitting `stage: deploy` from the job will cause Gitlab to infer the `Test` stage.  But any job named `pages` will implicitly triggers a `pages:deploy` job, too.

```
pages:
  script:
    - echo "This cannot be empty"
  artifacts:
    paths:
    - public
```

---

### Example with HTML linting

This uses a Python image, downloads a Python-based HTML linter, and runs against the `public/index.html` file you created.  Also note that this will only run against the `master` branch.

```
image: python:3

pages:
  script:
    - pip install html-linter
    - (! (html_lint.py public/*.html | grep Error)) # Explanation below
  artifacts:
    paths:
    - public
  only:
  - master
```

A middle-out explanation:

```
    html_lint.py public/*.html                --> Lint all `.html` files in the `public` directory
                               | grep Error   --> Return successful if "Error" in the linter output (because that's what grep does)
 !(                                        )  --> Invert the exit code of this group of commands.  Return failure if we found "Error"
(                                           ) --> Use a Bash sub-shell to make the whole thing palatable for GitLab
```

---

|Previous: [Tests at Gitlab](/labs/03_tests_at_gitlab.md)|Next: [TDD and Teams](/labs/05_tdd_and_teams.md)|
|---:|:---|
