# The Scenario

Your organization struggles with release cycles.  Software features are under development for weeks, and there's usually a code freeze the week before a scheduled release.  There should be enough time for QA to go through everything during the code freeze, but it's not uncommon to reschedule a release because things aren't ready to go.  This is usually due to unforeseen consequences when the code from different feature branches is brought together in the integration environment.

Because feature releases are infrequent events, they build up an inventory of bug fixes and features that customers integrate into their own business plans.  When a release schedule slips, it's an embarrassment to the organization that leaves customers frustrated.  This means that software development, quality assurance, and operations frequently work long hours to try and deliver.  Unfortunately, it feels like everything is just slipping further behind.

Fortunately, you have a new CTO who thinks she can turn things around.  There's a new project on the horizon, and she's creating a tiger team to build out a reference delivery pipeline to use on the new project.  The project starts in two weeks, which means this team will have to work quickly.

Lucky you!  You've been selected!

> Hello,
>
> I'd like to make sure that you're all set with everything you'll need when the team starts working together next Monday.  Please make sure that you have git installed and that you've created a GitLab account.
>
> Thank you!
>
> Jane
>
> CTO

----

### Stuff to do

1. Make sure you have `git` installed on your machine (run `git --version`).
5. If you don't already have one, create a free account at [GitLab.com](https://gitlab.com)

---

|Previous: [Top Level](/Readme.md)|Next: [First Tests](/labs/02_first_tests.md)|
|---:|:---|
