# TDD, CI, and Teams

> I like what you've got so far.  But I'm worried that it won't reasonably scale out across a user base.  I can just see someone trying to practice TDD principles and breaking mainline for everyone!  Can you show me a distributed model that keeps the default branch in good working order?

---

### Stuff to do

5. *Clone* the [shared demo repo](https://gitlab.com/aayore/sfs-gitlab-ci-shared-demo)
5. Create your own feature branch from master
  - `git checkout master`
  - `git pull`
  - `git checkout -b ${USER}-feature`
  - `git push --set-upstream origin ${USER}-feature`
5. Add (& commit!) a test that
  - Is currently failing
  - Will pass when your feature code is complete
5. Write (& commit!) code to make your test pass
5. MAKE SURE YOUR TEST PASSES!
5. Merge\* your code back to the master branch
  - `git checkout master`
  - `git pull`
  - `git merge ${USER}-feature`
5. Commit (push) the master back to the server
5. Clean up your feature branch
  - `git branch -d ${USER}-feature`
5. Repeat!

---

\* This could be done as a Merge Request.  When considering whether to make Merge Requests part of your pipeline...

+ Positives
  - You can automatically delete your feature branch
  - Opportunity for peer review of code
  - GitLab hooks allow for things like "auto-merge if pipeline succeeds"
  - Can be very good if there is a hierarchy of developers (e.g. a public, open-source project where contributions need to be vetted)
- Negatives
  - Slows things down
  - Not native git functionality
  - Have to switch into the GitLab web UI
  - You usually can't create a Merge Request an IDE (AFAIK)
  - You can't create a Merge Request from the CLI (without using API calls)

---

|Previous: [Deployment Automation](/labs/04_deployment_automation.md)|Next: [Main](/Readme.md)|
|---:|:---|
