# Automating the Tests

> It's pretty inconvenient to run those tests manually every time you change something.  And we don't want everything local on your machine, either.  Can you set it up so those tests will run every time the code is checked in to GitLab?

---

### Stuff to do

1. Save the following as `.gitlab-ci.yml` in your repo:

        image: alpine:3.7

        stages:
          - build
          - unit tests
          - deploy

        build_app:
          stage: build
          script:
            - echo "this is where you would build your app"
            - echo "if your app needed building"

        unit_tests:
          stage: unit tests
          script:
            - sh ./unit_test.sh

        ship_it:
          stage: deploy
          script:
            - echo "Deploying to PROD!"


3. Add a stage and a job for your integration tests.
5. `git push` your repo and make sure everything runs like it should.

##### Double-Check: Did you add the job & stage for integration tests?

---

|Previous: [First Tests](/labs/02_first_tests.md)|Next: [Deployment Automation](/labs/04_deployment_automation.md)|
|---:|:---|
