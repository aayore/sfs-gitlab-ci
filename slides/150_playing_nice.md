# Playing Nice With Others

An even simpler model:

    +--------------------.
    |                     \      
    +--> feature --\       \      
    |               \       \      
    +--> feature ----*--> master --.
    |               /              |
    +--> feature --/               V
            |               Tests and deploys to PROD
            |
            +--> Only tests
