# Lies you will hear about CI

- Never break the build

- If you're branching, it's not real CI

- If you're not deploying to production with every commit, you're doing it wrong

<br><br><br>


# Presenter Notes

- `Never break the build` --> `Never break the build artifact`

- No branching...

  - Means testing prior to submitting work to a CI system

  - This can work for small (e.g. 1 dev) teams, but not larger teams.

  - Branches can exist to parallel environments

- Don't confuse CI with CD
