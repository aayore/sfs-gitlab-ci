# Different Types of Tests

[Unit Tests](https://en.wikipedia.org/wiki/Unit_testing)

  - Smallest, earliest testing.  Tests a unit of code in isolation.

[Integration Tests](https://en.wikipedia.org/wiki/Integration_testing)

  - Testing how the code integrates into a production-like environment.

  - NOT THE SAME "INTEGRATION" AS IN CONTINUOUS INTEGRATION!

[Load Tests](https://en.wikipedia.org/wiki/Load_testing)

  - Testing code under an extreme load, to find weak points in the code or infrastructure.

# Presenter Notes

Unit tests don't include network, database, etc.

  - A front end application won't have any back-end services.

Integration tests DO include network, database, etc.

All tests will have to be customized to your application.
