# Resources

- [_Continuous Integration_ by Martin Fowler](https://martinfowler.com/articles/continuousIntegration.html)

- [Rubber Chicken CI](http://www.jamesshore.com/Blog/Continuous-Integration-on-a-Dollar-a-Day.html)

- [GitLab Flow](https://docs.gitlab.com/ce/workflow/gitlab_flow.html) - I like this much better than [git-flow](https://danielkummer.github.io/git-flow-cheatsheet/)

- [.gitlab-ci.yml reference](https://docs.gitlab.com/ce/ci/yaml/README.html)
