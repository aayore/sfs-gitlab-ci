# Build

What does *build* mean?

  - Scripts: Maybe nothing.

  - NodeJS: `npm build`

  - Scala: `sbt build`

  - Docker: `docker build ${context}`

  - Infrastructure: ???

  - Configuration management modules: ???
