# Best?  Easiest?

Optimized Development/Test/Secure/.../Deploy Practices:

  - Smaller commits --> less risk, easier troubleshooting

  - Frequent commits --> frequent testing

  - Automation --> repeatable, codified standards

  - Less WIP --> Lower the bottom line

  - Push code --> Stuff happens --> Easy!

<br><br>