# Workflow Modeling

Need to be deliberate with a workflow model

- [GitLab Flow](https://about.gitlab.com/2014/09/29/gitlab-flow/) with environment branches is my favorite

What does your team structure look like?

What do your environments look like?

Test everything.

Make the pipeline as fast as possible.
