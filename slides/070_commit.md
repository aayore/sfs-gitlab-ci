# Commit

I will use "commit" three (at least) different ways

(only one of which is technically correct)

    !python
    'Commit your changes' = 'git commit -m \'description_of_changes\''

    'Commit your changes to the server' = 'git push'

    'Commit your changes to master' = 'git checkout master && git merge my_branch'

Please stop me to ask for clarification if it's unclear

Questions?
