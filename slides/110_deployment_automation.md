# Deployment Automation

Sometimes "deploy" means "publish"

- Push a script to central storage

- Push a container image to a registry

And sometimes it means "publish, then deploy"

- Push a container image to a registry

- Then tell a Kubernetes service to update

And sometimes it just means "deploy"...
