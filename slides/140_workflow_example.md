# Workflow Example

    .-----------------------.
    |                       |      +-- manually gated? automated?
    +--> feature --\        |      |
    |               \       |      V
    +--> feature ----*--> master --O--> production
    |               /       |              |
    +--> feature --/        |              +--> Deploys to Production
            |               |
            |               +--> Deploys to QA
            |
            +--> Deploys to DEV

# Presenter Notes

Create a feature branch from master
- Committing WIP directly to master interferes with TDD
Use the CI system to test your feature build
- If this deploys to a shared environment, race conditions will exist
- There could (will) be a lot of thrashing in this environment
When you're feature-complete and all the tests are passing
- Merge latest updates from master
- Merge your branch to master
