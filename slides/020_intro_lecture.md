# Continuous Integration

Is a workflow model

  - Frequent commits

  - Automated testing

  - Minimal inventory

Enables faster, safer, distributed work

Need team buy-in

  - Development

  - Operations

# Presenter Notes

Development

  - Faster bug identification

  - Easier bug fixes

Operations

  - Design the pipeline for operational best practices

Faster security patching

Transparent, documented processes - with no docs!
