# Testing Frameworks

There are [SO MANY](https://en.wikipedia.org/wiki/List_of_unit_testing_frameworks)!

Some to note:

  - [Kitchen](https://docs.chef.io/kitchen.html) for Chef.

  - Django's [tutorial](https://docs.djangoproject.com/en/2.0/intro/tutorial01/) introduces automated testing in part 5.

  - [JMeter](https://jmeter.apache.org/) from Apache is a good load testing tool.

<br><br><br><br><br><br>
