# Darkslide

This presentation was created using [darkslide](https://github.com/ionelmc/python-darkslide).
<br>
Choose a theme:<br>[default](index.html#slide:3) | [abyss](abyss.html#slide:3) | [void](void.html#slide:3) | [white](white.html#slide:3)
