Continuous Integration (using GitLabCI)

What is it?
Continuous integration is the process of integrating individuals' code changes into the distributed workflow as frequently and safely as possible.

- Build changes as soon as we know about them.
- Test the builds as quickly as we can.
- Automate deployments.


Theory:
Integrating our changes as frequently as possible makes for a better product.

- Smaller changes mean less can go wrong.
- Frequent testing means less backtracking.
- An up-to-date integration environment means earlier collision detection.


Who:
This is important for everyone involved in the delivery of changes to production.  Developers, software testers, systems administrators, etc.

Developers:
- Bugs can be found earlier, which leads to easier troubleshooting.
- Integration environment will be more up-to-date with changes from other team members.

QA:
- Creating and maintaining automated tests help increase value of QA personnel.
- Tests are executed as part of a workflow, thereby removing the end-of-sprint backlog on QA.

Systems administrators / SRE:
- A standard CI process can yield deployment artifacts that are easier to manage.
- Systems changes can be managed through the same change process.

A little bit of up-front investment in this process starts yielding productivity returns quickly.

Ownership of continuous integration can be muddled due to the number of stakeholders.  In smaller organizations, developers (a.k.a. Full Stack Engineers) may own this process tip to tail.  As organizations become larger, different groups will need to collaborate.  In my own experience, developer involvement usually drops off quickly, and this becomes a service provided to developers by operations and/or devops.  In that case, it requires the heavy involvement of QA to implement the test automation.


https://docs.gitlab.com/ce/ci/img/cicd_pipeline_infograph.png


Continuous Integration vs. Continuous Delivery vs. Continuous Deployment
- Continuous integration is the process of building and testing code.
- Continuous delivery is the process of delivering a product to a release manager who manages the production deployment.
- Continuous deployment means that any changes that pass all the automated tests will automatically be deployed to production, without human intervention.


Deployment artifacts:
Modern software is trending toward distributed, horizontally-scalable (ideally elastic), service-based architecture.
Distributed means that we have to run on more systems than ever before.
Horizontally-scalable means that we need to be able to automatically provision systems and code.
Service-based architecture - specifically micro-services - means that we're going to have more instances of code to deploy.

From an operations standpoint, this means that custom, copy-this-here-and-that-there deployments are less tenable than ever before.  In order to scale with your organization, you need to standardize and automate.  Ownership over some or all of the CI/CD is your opportunity for this.  By creating an automated, modular system, you can ensure the CI/CD pipeline yields a standard, manageable deployment artifact.

Containers are my weapon of choice.  I believe the industry is embracing them because containers are:
1. More portable than VM's, rpm's, deb's, exe's, or msi's.
2. More inclusive than anything in that list except VM's.
3. Lighter-weight than VM's.
4. Capable of better compaction than VM's.
5. Complicated to orchestrate, but more manageable than the others with modern tooling.

That said, if your environment already has a standard - or significant inroads to a standard - that isn't containers, one of the other options may be a better fit for your organization.


Testing:
- Unit - Is this module fit for use?
- Integration - Does this module play well with others?
- Regression - Does this change break previous functionality?
- Validation - Does this change add the desired functionality?

Test-driven development: Writing the tests before writing the code.


Labs:
0. Draw out (in meatspace) a workflow/pipeline for Software X.
  - Software is a RESTful API (web app).
  - We want to standardize on Docker for production deployments.
  - GitLab for CI/CD.
  - WHAT IS THE DEPLOY TARGET?  GITLAB PAGES DOESN'T OFFER DEV/QA/UAT/PROD.
  - Who is involved in the process?  At which points?
  - When/where do the tests run?
1. Create a project in GitLab and clone it locally.
  - NEED A TEMPLATE WEB APP THAT'S EASY TO ADD ROUTES.
2. Build the code
3. Manually verify the build
4. Create automated test script
